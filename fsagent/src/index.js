const dotenv = require('dotenv');
const FolderScanner = require('./folder-scanner/');
const { restoreOrCreate, removeDocument } = require('./actions/');

dotenv.config();

const scanner = new FolderScanner({
  path: process.env.STORAGE_PATH,
  add: restoreOrCreate,
  remove: removeDocument,
});

scanner.start();

process.stdout.write(`Running in: ${process.env.NODE_ENV}`);
