const pdf = require('pdf-thumbnail');

module.exports = function generateThumbnail(buf) {
  return new Promise((resolve, reject) => {
    pdf(buf, {
      resize: {
        height: 360,
        width: 257,
      },
    }).then((stream) => {
      const parts = [];
      stream.on('data', (chunk) => {
        parts.push(chunk);
      });

      stream.on('end', () => {
        resolve(Buffer.concat(parts));
      });

      stream.on('error', (err) => {
        reject(Error(`Unable to generate thumbnail: ${err}`));
      });
    })
      .catch(reject);
  });
};
