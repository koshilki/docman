const thumbnail = require('./thumbnail');
const { pathToName, md5 } = require('./utils');

module.exports = {
  thumbnail,
  pathToName,
  md5,
};
