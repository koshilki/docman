const md5file = require('md5-file');

const pathToName = (path) => path.split(/[/\\]/).pop();

const md5 = (path) => md5file.sync(path);

module.exports = {
  pathToName,
  md5,
};
