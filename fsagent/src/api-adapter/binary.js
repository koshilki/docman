const axios = require('axios');

const create = async ({ data, contentType }) => {
  const res = await axios.post('http://api:3000/api/binaries', {
    data, contentType,
  });

  return res.data.id;
};

module.exports = {
  create,
};
