const axios = require('axios');

const create = async ({
  name, path, hash, preview,
}) => {
  const res = await axios.post('http://api:3000/api/documents', {
    name, path, hash, preview,
  });

  return res.id;
};

const remove = async (path) => {
  const { data } = await axios.get(`http://api:3000/api/documents/search?path=${encodeURIComponent(path)}`);
  const tasks = [];

  // Remove all documents with specified path
  data.forEach((doc) => {
    tasks.push(axios.delete(`http://api:3000/api/documents/${doc._id}`));
  });
  return Promise.all(tasks);
};

const update = async (id, data) => axios.put(`http://api:3000/api/documents/${id}`, data);

const findByHash = async (hash) => {
  const { data: doc } = await axios.get(`http://api:3000/api/documents/search?hash=${hash}`);

  return doc && doc[0];
};

module.exports = {
  create,
  remove,
  update,
  findByHash,
};
