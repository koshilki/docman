const document = require('./document.js');
const binary = require('./binary.js');

module.exports = {
  document,
  binary,
};
