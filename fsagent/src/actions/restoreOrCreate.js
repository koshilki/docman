const { md5: md5file } = require('../common/');
const { document } = require('../api-adapter/');
const restoreDocument = require('./restoreDocument');
const createDocument = require('./createDocument');

module.exports = async function restoreOrCreate(path) {
  const md5 = md5file(path);

  const doc = await document.findByHash(md5);

  if (doc) {
    return restoreDocument(path, doc);
  }

  return createDocument(path);
};
