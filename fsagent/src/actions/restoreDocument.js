const { document } = require('../api-adapter/');
const { pathToName } = require('../common/utils');

module.exports = async function restoreDocument(path, doc) {
  const restored = {
    ...doc,
    path,
    name: pathToName(path),
    removed: false,
  };

  return document.update(doc._id, restored);
};
