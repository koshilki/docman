const createDocument = require('./createDocument');
const restoreOrCreate = require('./restoreOrCreate');
const removeDocument = require('./removeDocument');

module.exports = {
  restoreOrCreate,
  createDocument,
  removeDocument,
};
