const fs = require('fs');
const { thumbnail, pathToName, md5: md5file } = require('../common');
const { document, binary } = require('../api-adapter');

module.exports = async function createDocument(path) {
  const md5 = md5file(path);
  const name = pathToName(path);

  // readFile
  const fileData = fs.readFileSync(path);
  // generateThumbnail
  const thumbData = await thumbnail(fileData);

  // saveThumbnailBinary
  const preview = await binary.create({
    data: thumbData,
    contentType: 'image/jpeg',
  });

  // saveDocument
  const doc = await document.create({
    name,
    hash: md5,
    path,
    preview,
  });

  /*
  OCR
  saveOCRData
  */

  return doc;
};
