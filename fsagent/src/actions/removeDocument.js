const { document } = require('../api-adapter');

module.exports = async function removeDocument(path) {
  return document.remove(path);
};
