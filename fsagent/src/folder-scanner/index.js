const chokidar = require('chokidar');

const logHandler = (action) => (path) => {
  process.stdout.write(`${action}: ${path}`);
};

class FolderScanner {
  constructor({
    path = '/tmp', add = logHandler('add'), remove = logHandler('remove'), change = logHandler('change'), error = logHandler('error'),
  }) {
    this.path = path;
    this.addHandler = add;
    this.removeHandler = remove;
    this.changeHandler = change;
    this.errorHandler = error;
  }

  start() {
    this.watcher = chokidar.watch(this.path, {
      usePolling: true,
      ignoreInitial: true,
    });
    this.watcher
      .on('add', this.addHandler)
      .on('change', this.changeHandler)
      .on('unlink', this.removeHandler)
      .on('error', this.errorHandler);
  }

  stop() {
    this.watcher.close();
  }
}

module.exports = FolderScanner;
