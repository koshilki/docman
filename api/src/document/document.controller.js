const Document = require('./document.model');

const DEFAULT_PER_PAGE = 20;

const ALLOWED_SEARCH_KEYS = [
  'path',
  'hash',
  'name',
];

module.exports.check = async (req, res, next) => {
  const document = await Document.findById(req.params.id);
  if (!document) {
    throw Error('Document not found');
  }

  next();
};

module.exports.create = async (req, res) => {
  const document = new Document(req.body);
  await document.save();

  res.json(document);
};

module.exports.remove = async (req, res) => {
  const doc = await Document.findById(req.params.id);
  doc.removed = true;
  await doc.save();

  res.json(req.params.id);
};

module.exports.list = async (req, res) => {
  const pp = parseInt(req.query.pp || DEFAULT_PER_PAGE, 10);
  const pn = parseInt(req.query.pn ? (req.query.pn - 1) : 0, 10);

  const documents = await Document.find().where('removed').ne(true).skip(pp * pn)
    .limit(pp);

  res.json(documents);
};

module.exports.update = async (req, res) => {
  const document = await Document.findOneAndUpdate({ _id: req.params.id }, req.body, {
    new: true,
  }).exec();

  res.json(document);
};

module.exports.view = async (req, res) => {
  const document = await Document.findById(req.params.id);

  res.json(document);
};

module.exports.search = async (req, res) => {
  const searchQuery = {};
  for (const [key, value] of Object.entries(req.query)) {
    if (ALLOWED_SEARCH_KEYS.indexOf(key) > -1) {
      searchQuery[key] = value;
    }
  }
  const documents = await Document.find(searchQuery).exec();

  res.json(documents);
};
