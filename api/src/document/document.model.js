const mongoose = require('mongoose');

const DocumentSchema = new mongoose.Schema({
  name: String,
  created: Date,
  updated: Date,
  path: String,
  hash: String,
  preview: { type: mongoose.Schema.Types.ObjectId, ref: 'Binary' },
  removed: Boolean,
});

const Document = mongoose.model('Document', DocumentSchema);

module.exports = Document;
