const bodyParser = require('body-parser');
const cors = require('@robertoachar/express-cors');
const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');

const { catchAll, notFound } = require('./error');

const app = express();
const userRouter = require('./user/user.router');
const documentRouter = require('./document/document.router');
const binaryRouter = require('./binary/binary.router');

app.use(helmet());
app.use(cors());
app.use(morgan('tiny'));
app.use(bodyParser({ limit: '1mb' }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (req, res) => {
  res.json({ message: 'It works!' });
});

app.use('/api/users', userRouter);
app.use('/api/documents', documentRouter);
app.use('/api/binaries', binaryRouter);

app.use(notFound);
app.use(catchAll);

module.exports = app;
