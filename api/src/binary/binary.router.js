const express = require('express');
const catchErrors = require('express-catch-errors');

const router = express.Router();
const {
  check,
  create,
  remove,
  list,
  update,
  view,
  download,
} = require('./binary.controller');

router
  .route('/')
  .get(catchErrors(list))
  .post(catchErrors(create), catchErrors(create));

router
  .route('/:id')
  .get(catchErrors(check), catchErrors(view))
  .put(catchErrors(check), catchErrors(update))
  .delete(catchErrors(check), catchErrors(remove));

router
  .route('/:id/download')
  .get(catchErrors(check), catchErrors(download));

module.exports = router;
