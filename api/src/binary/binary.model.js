const mongoose = require('mongoose');

const BinarySchema = new mongoose.Schema({
  data: Buffer,
  contentType: String,
});

const Binary = mongoose.model('Binary', BinarySchema);

module.exports = Binary;
