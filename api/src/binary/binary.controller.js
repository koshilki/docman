const Binary = require('./binary.model');

module.exports.check = async (req, res, next) => {
  const binary = await Binary.findById(req.params.id);
  if (!binary) {
    throw Error('Document not found');
  }

  next();
};

module.exports.create = async (req, res) => {
  const binary = new Binary({
    data: req.body.data,
    contentType: req.body.contentType,
  });
  await binary.save();

  res.json({ status: 'OK', id: binary.id });
};

module.exports.remove = async (req, res) => {
  await Binary.findByIdAndRemove(req.params.id);

  res.json(req.params.id);
};

module.exports.list = async (req, res) => {
  const binaries = await Binary.find();

  res.json(binaries);
};

module.exports.update = async (req, res) => {
  const binary = await Binary.findOneAndUpdate({ _id: req.params.id }, req.body, {
    new: true,
  }).exec();

  res.json(binary);
};

module.exports.view = async (req, res) => {
  const binary = await Binary.findById(req.params.id);

  res.json(binary);
};

module.exports.download = async (req, res) => {
  const binary = await Binary.findById(req.params.id);

  res.header({ 'content-type': binary.contentType });
  res.send(binary.data);
};
