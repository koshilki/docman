import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import DocumentListItem from './DocumentListItem';

const styles = theme => ({
});

class DocumentList extends Component {
  state = {
    docs: []
  };

  classes = [];

  constructor(props) {
    super();

    this.classes = props.classes;
  }

  render() {
    return (
      <React.Fragment>

        {this.state.docs.map(({ name, type, _id: id, preview }) => (
          <DocumentListItem key={id} name={name} mimetype={type} id={id} previewId={preview}></DocumentListItem>
        ))}
      </React.Fragment>
    );
  }

  componentDidMount() {
    fetch(`http://localhost:3000/api/documents`).then(async (response) => {
      try {
        this.setState({ docs: await response.json() });
      } catch (e) {
        console.log('Unable to get docs', e);
      }
    });
  }
}

DocumentList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DocumentList);
