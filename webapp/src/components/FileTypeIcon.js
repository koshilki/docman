import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  toolbar: {
    minHeight: 64,
  },
  file: {
    width: 20,
    height: 28,
    backgroundColor: 'rgba(255,0,0,.5)',
    position: 'relative',
    display: 'inline-block',
    borderRadius: 1,
  },
  corner: {
    position: 'absolute',
    top: 0,
    right: 0,
    borderTop: '5px solid white',
    borderLeft: '5px solid red',
    width: 0,
  },
  extension: {
    bottom: 6,
    display: 'inline-block',
    padding: '1px 2px',
    position: 'absolute',
    textAlign: 'center',
    fontSize: 9,
    fontWeight: 500,
    borderRadius: 1,
    marginLeft: -1,
    boxShadow: '0 1px 1px rgba(0,0,0,.5)'
  },
  pdf: {
    backgroundColor: 'red',
    color: 'white'
  }
});

const typeMapping = {
  'application/pdf': {
    ext: 'pdf',
  }
}

const getExtension = mimetype => typeMapping[mimetype].ext.toUpperCase();

function FileTypeIcon(props) {
  const { classes, mimetype } = props;

  return (
    <React.Fragment>
      <div className={classes.file}>
        <div className={classes.corner}></div>
        <div className={[classes.extension, classes[typeMapping[mimetype].ext]].join(' ')}>{getExtension(mimetype)}</div>
      </div>
    </React.Fragment>
  )
}

export default withStyles(styles)(FileTypeIcon);
