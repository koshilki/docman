import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardHeader from '@material-ui/core/CardHeader';
import FileTypeIcon from './FileTypeIcon';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  card: {
    width: 257,
    borderRadius: 4,
    margin: theme.spacing(1),
    display: 'inline-block',
  },
  media: {
    height: 217,
    backgroundPosition: 'top'
  },
  created: {
    fontSize: 10,
    color: 'rgba(0,0,0,.5)'
  }
});

const getPreviewUrl = binaryId => {
  return `http://localhost:3000/api/binaries/${binaryId}/download`;
}

const trimExtension = name => name.replace(/\.[^.]+$/, '');

function DocumentListItem(props) {
  const { classes, name, type, previewId } = props;

  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={getPreviewUrl(previewId)}
          title={name}
        />
        <CardHeader
          avatar={
            <FileTypeIcon mimetype={type || "application/pdf"} />
          }
          title={trimExtension(name)}
        />
        <CardContent>
          <Grid container spacing={2} alignItems="center">
            <Grid item xs />
            <Grid item>
              <Typography variant="subtitle2" align="right" className={classes.created}>Created 01/01/2019</Typography>
            </Grid>
          </Grid>
        </CardContent>
      </CardActionArea>
    </Card >
  );
}

DocumentListItem.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DocumentListItem);
